<?php namespace server;

class setting {
    function __construct($file = null)
    {
        if ($file !== null) {
            // exti.
        } else {
            $root = dirname(__DIR__);
            $content = file_get_contents($root . "/configuration");
            $this->content = json_decode($content, true);
        }
    }
    static function env($name = null){
        if (is_null($name)) {
            return setting::getInstants()->content['environment'];
        } else {
            return setting::getInstants()->content['environment'][$name];
        }
    }
    static function database($name = null){
        $database = setting::getInstants()->content['environment']['database'];
        if (is_null($name)) {
            return setting::env($database);
        } else {
            return setting::env($database)[$name];
        }
    }
    static function getInstants(setting $object = null)
    {
        if ($object !== null) {
            // exit;
        } else {
            return new setting;
        }
    }
}