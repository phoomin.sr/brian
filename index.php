<?php

session_start();

use providers\routes\Route;
use providers\view\Views;

require_once dirname(__FILE__) . "/vendor/autoload.php";
require_once dirname(__FILE__) . "/providers/registers.php";
require_once dirname(__FILE__) . "/app/routes/web.php";

define("DB_HOST", $db_host);
define("DB_NAME", $db_name);
define("DB_USER", $db_user);
define("DB_PASSWORD", $db_password);
define("APPNAME", $appname);

$request->APPNAME = $appname;

function view($template, $data = array()) {
    return Views::render($template, $data);
}
function response(Array $response) {
    return json_encode($response);
}
function redirect_to($url) {
    return ['redirect' => $url];
}
function prep_search($string, $reverse = false) {
    if ($reverse) {
        $search = explode('.', $string);
    } else {
        $search = str_replace(" ", '.', $string);
    }
    
    return $search;
}
