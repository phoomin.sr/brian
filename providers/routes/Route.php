<?php namespace providers\routes;
require_once dirname(__DIR__, 2) . "/vendor/autoload.php";

use providers\view\Views;
use Phroute\Phroute\Exception\HttpRouteNotFoundException;
use Phroute\Phroute\Exception\HttpMethodNotAllowedException;
use providers\routes\RouteDispatcher;

class Route {
    public static function getInstant()
    {
        return new Route;
    }
    public function ProcessInput($uri)
    {
        $uri = implode('/', 
        array_slice(
                explode('/', $_SERVER['REQUEST_URI']), 2
            )
        );
        return $uri;
    }
    public function ProcessOutput($response)
    {
        if ($response instanceof Views) {
            echo $response->run();
        } elseif (isset($response['redirect'])) {
            header('location:' . $response['redirect']);
        } else {
            echo $response;
        }
    }
    public function getOutput($route)
    {
        $dispatcher = new RouteDispatcher($route->getData(), null, $route);
        try {
            $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $this->processInput($_SERVER['REQUEST_URI']));
        } catch (HttpRouteNotFoundException $e) {
            echo $e->xdebug_message;
            die();
        } catch (HttpMethodNotAllowedException $e) {
            echo $e->xdebug_message;       
            die();
        }
        $this->processOutput($response);
    }
} 