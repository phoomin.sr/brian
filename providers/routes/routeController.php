<?php namespace providers\routes;

use providers\routes\Route;
use Phroute\Phroute\Route as RouteRoute;
use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Exception\BadRouteException;
use Phroute\Phroute\RouteParser;
use server\setting;

class routeController extends RouteCollector
{
    public function get($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::GET, $route, $this->handler($handler), $filters);
    }
    public function post($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::POST, $route, $this->handler($handler), $filters);
    }
    public function update($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::PUT, $route, $this->handler($handler), $filters);
    }
    public function delete($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::DELETE, $route, $this->handler($handler), $filters);
    }
    public function any($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::ANY, $route, $this->handler($handler), $filters);
    }
    public function handler($handler)
    {
        if (is_array($handler)&&count($handler) == 1)
        {
            array_push($handler, 'run_func_bymethod');
        }

        return $handler;
    }
    /**
     * @param $name
     * @param array $args
     * @return string
     */
    public function route($name, array $args = null)
    {
        $url = [];

        $replacements = is_null($args) ? [] : array_values($args);

        $variable = 0;

        if (property_exists($this, 'reverse')) {
            foreach($this->reverse[$name] as $part)
            {
                if(!$part['variable'])
                {
                    $url[] = $part['value'];
                }
                elseif(isset($replacements[$variable]))
                {
                    if($part['optional'])
                    {
                        $url[] = '/';
                    }

                    $url[] = $replacements[$variable++];
                }
                elseif(!$part['optional'])
                {
                    throw new BadRouteException("Expecting route variable '{$part['name']}'");
                }
            }
        } else {
            $url = array_merge([$name], $replacements);
        }

        $prefix = setting::env('production') == "production" ? "https://":"http://";
        $location = setting::env('location');

        return $prefix . $location . '/' .  implode('/', $url);
    }
    static function getRoute(String $path, Array $params = array())
    {
        $url = array_merge([$path], $params);
        $prefix = setting::env('production') == "production" ? "https://":"http://";
        $location = setting::env('location');
        
        return $prefix . $location . '/' .  implode('/', $url);
    }
    public function __destruct()
    {
        $Routes = new Route;
        $Routes->getOutput($this);
    }
}