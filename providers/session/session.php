<?php namespace providers\session;

class session {
    public function __construct($Appname)
    {
        $this->Appname = $Appname;
        if (isset($_SESSION[$Appname])) {
            $this->session = $_SESSION[$Appname];
        } else {
            $this->session = array();
        }
    }
    final function set($name, $value) {
        $this->session[$name] = $value;
        $this->render();
        return $this;
    }
    final function get($name) {
        if (isset($this->session[$name])) {
            $res = $this->session[$name];
        } else {
            $res = null;
        }
        return $res;
    }
    final function render() {
        $_SESSION[$this->Appname] = $this->session;
    }
    final function unset() {
        unset($_SESSION[$this->Appname]);
    }
}