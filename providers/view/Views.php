<?php namespace providers\view;

use Exception;
use eftec\bladeone\BladeOne;
use providers\routes\routeController;

class Views {
    public static $path = "/resources/views";
    public static $castpath = "/storages/views";
    public static $globaldata = array();
    public $templatename = "";

    public static function getInstants()
    {
        return new Views;
    }
    public static function Connect(Views $obj)
    {
        $obj->blade = new BladeOne(dirname(__DIR__, 2) . Views::$path, dirname(__DIR__, 2) . Views::$castpath);
        return $obj;
    }
    public static function render($view, $data) 
    {
        $views = Views::getInstants();
        $views = Views::Connect($views);
        if (!isset($data['title'])) {
            $data['title'] = APPNAME;
        }
        $views->blade->setView($view)->share($data);
        return $views;
    }
    public static function ResponseJson($view, $data)
    {
        $view = Views::getInstants();
        Views::Connect($view);
        return json_encode($view->blade->setView($view)
            ->share($data)
            ->share(self::$globaldata)
            ->blade_render());
    }
    public function Auth($name = "", $auth = "", $condition = "")
    {
        $this->blade->setAuth($name, $auth, $condition);
        return $this;
    }
    public function run()
    {
        $this->setOptions();
        return $this->blade->run();
    }
    public function setOptions()
    {
        $this->blade->setAliasClasses([
            'record' => 'controllers\record',
            'route' => 'providers\routes\routeController'
        ]);
    }
}