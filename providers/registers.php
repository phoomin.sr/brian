<?php

use server\configuration;
use server\setting;

require_once dirname(__DIR__) . "/vendor/autoload.php";

extract([
    "Routes" => new \providers\routes\Route,
    "tables" => new \providers\tables\tables,
    "request" => new \providers\request\Request,
    "router" => new \providers\routes\routeController(new Phroute\Phroute\RouteParser),
    "view" => new \providers\view\Views,
    "db_host" => setting::database('network'),
    "db_user" => setting::database("database_username"),
    "db_password" => setting::database('database_password'),
    "db_name" => setting::database('database_name'),
    "appname" => setting::env('site_name')
]);