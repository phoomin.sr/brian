<?php namespace providers\models;

use PDO;
use Exception;
use myLog;
use DB;

class models {
    protected $db_name;
    protected $db_host;
    protected $db_user;
    protected $db_password;
    protected $dev = true;

    const ORDER_TYPE = [
        'a-z' => 'ASC',
        'z-a' => 'DESC'
    ];
    protected $lexpl = "AND";

    function connect () {
        return new PDO("mysql:host=".$this->db_host.";dbname=".$this->db_name.";charset=utf8", $this->db_user, $this->db_password);
    }
    function select($slt)
    {
        $args = func_num_args();
        for ($i = 0;$i < $args;$i++) {
            $params[] = func_get_arg($i);
        }

        $this->select = implode(", \r\n", $params);

        return $this;
    }
    function insert($data)
    {
        if (property_exists($this, 'fillable')) {
            $this->bindValue = array();
            $column = [property_exists($this, 'default_id') ? $this->default_id:"id"];
            $idDefaultValue = NULL;
            $column = array_merge($column, $this->fillable);
            $values = array();

            foreach ($column as $column_name) {
                $colId = property_exists($this, 'default_id') ? $this->default_id:"id";
                if ($column_name == $colId) {
                    $values[$column_name] = '?';
                    $this->bindValue[] = $idDefaultValue;
                } else {
                    $values[$column_name] = '?';
                    $this->bindValue[] = $data[$column_name]; 
                }
            }

            if (property_exists($this, 'timestamps')) {
                $this->bindValue[] = date('Y-m-d H:i:s');
                $this->bindValue[] = date('Y-m-d H:i:s');
                $column[] = 'create_at';
                $values[] = '?';
                $column[] = 'update_at';
                $values[] = '?';
            }

            $im_column = implode(', ', $column);
            $im_values = implode(', ', $values);

            $insert = "INSERT INTO $this->table ($im_column) VALUES ($im_values)";
            
            $conn = $this->connect();
            $stmt = $conn->prepare($insert);

            if (property_exists($this, 'bindValue')) {
                for($i = 0;$i < count($this->bindValue);$i++){
                    $stmt->bindParam($i + 1, $this->bindValue[$i]);
                }
            }

            if ($stmt->execute()) {
                return $conn->lastInsertId();
            } else {
                var_dump($insert, $this->bindValue);
                die;
            }
        }
    }
    function update($data = array())
    {
        $table = $this->table;
        $idDefaultValue = "NULL";

        foreach ($data as $key => $value) {
                $set_values[] = "$key = ?";
                $this->bindValue[] = $data[$key];
        }

        if (property_exists($this, 'timestamps')) {
            $set_values[] = "update_at = ?";
            $this->bindValue[] = date('Y-m-d');
        }

        $im_value = implode(", \r\n", $set_values);
        $this->stm = "UPDATE $table SET $im_value";

        return $this;
    }
    function delete()
    {
        $this->stm = "DELETE FROM ".$this->table;

        return $this;
    }
    function find($id = "")
    {
        $slt = property_exists($this, 'select') ? $this->select:"*";
        $column_id = property_exists($this, 'default_id') ? $this->default_id:"id";

        if ($id !== "") {
            $this->stm = "SELECT\r\n\t $slt\r\n FROM\r\n\t" . $this->table;
            $this->where($column_id, '=', $id);
            return $this->get()[0];
        } else {
            $this->stm = "SELECT\r\n\t $slt\r\n FROM\r\n\t" . $this->table;
        }

        return $this;
    }
    function insertOrupdate($field, $id, $data = array())
    {
        $get = count($this
        ->where($field, '=', $id)
        ->where(array_keys($data)[0], '=', $data[array_keys($data)[0]])
        ->get());
        unset($this->bindValue);
        unset($this->where);

        if ($get == 0) {
            $this->insert(array_merge([$field => $id], $data));
        } else {
            $this->update($data)
            ->where($field, '=', $id)
            ->where(array_keys($data)[0], '=', $data[array_keys($data)[0]])
            ->get();
        }
    }
    function raw($raw)
    {
        if (property_exists($this, 'stm')) {
            $this->stm .= $raw;
        } else {
            $this->stm = $raw;
        }

        return $this;
    }
    function where($column, $express = "", $value)
    {
        if (property_exists($this, 'where')) {
            $this->where .= " $this->lexpl\r\n\t$column $express ";
        } else {
            $this->where = " $column $express ";
        }

        if (is_callable($value)) {
            $this->bindValue[] = $value();
        } else {
            $this->bindValue[] = $value;
        }
        $this->where .= "?";
        return $this;
    }
    function join($table, $on, $cluse, $type = null)
    {
        if (property_exists($this, 'join')) {
            $this->join .= ' '.strtoupper($type)." JOIN\r\n\t".$table."\r\nON\r\n\t".$on." = ".$cluse."\r\n";
        } else {
            $this->join = strtoupper($type)." JOIN\r\n\t".$table."\r\nON\r\n\t".$on." = ".$cluse."\r\n";
        }
        return $this;
    }
    function groupby()
    {
        $args = func_num_args();
        for ($i = 0;$i < $args;$i++) {
            $params[] = func_get_arg($i);
        }

        $colTogroup = implode(', ', $params);

        $this->group = "\r\nGROUP BY\r\n\t".$colTogroup;
        return $this;
    }
    function order ()
    {
        $args = func_num_args();
        $order = "";
        for ($i = 0;$i < $args;$i++) {
            if (func_get_arg($i) == 'a-z'||func_get_arg($i) == 'z-a') {
                $order = self::ORDER_TYPE[func_get_arg($i)];
            } else {
                $params[] = func_get_arg($i);
            }
        }

        $colToOrder = implode(', ', $params);
        $this->order = "\r\nORDER BY\r\n\t".$colToOrder." $order";

        return $this;
    }
    function concat()
    {
        $args = func_num_args();
        for ($i = 0;$i < $args;$i++) {
            $params[] = func_get_arg($i);
        }

        $colTogroup = implode(',', $params);

        return "CONCAT(".$colTogroup.")";
    }
    function group_concat()
    {
        $args = func_num_args();
        for ($i = 0;$i < $args;$i++) {
            $params[] = func_get_arg($i);
        }

        $colTogroup = implode(',', $params);

        return "GROUP_CONCAT(".$colTogroup.")";
    }
    function sum($column)
    {
        return "SUM($column)";
    }
    static function as($table , $as)
    {
        return $table. ' AS ' . $as; 
    }
    function limit($limit, $offset = 0)
    {
        $this->limit = " $limit OFFSET $offset";
        return $this;
    }
    final function get()
    {
        try{	
            // execute PDO query
            if (!property_exists($this, 'stm')) {
                $this->find();
            }
            if (property_exists($this, "join")) {
                $this->stm .= "\r\n" . $this->join;
            }
            if (property_exists($this, 'where')) {
                $this->stm .= "\r\nWHERE\r\n\t" . $this->where;
            }
            if (property_exists($this, 'group')) {
                $this->stm .= "\r\n" . $this->group;
            }
            if (property_exists($this, 'order')) {
                $this->stm .= "\r\n" . $this->order;
            }
            if (property_exists($this, 'limit')) {
                $this->stm .= "\r\nLIMIT\r\n" . $this->limit;
            }
            if (property_exists($this, "stm")) {
                $stmt = $this->connect()->prepare($this->stm);
                if (property_exists($this, 'bindValue')) {
                    for($i = 0;$i < count($this->bindValue);$i++){
                        $stmt->bindParam($i + 1, $this->bindValue[$i]);
                    }
                }
                unset($this->stm);
            }
           
            if ($stmt->execute()) {
                return $this->fetch($stmt);
            } else {
                var_dump($stmt, $this->bindValue);
                die;
            }
        } catch (Exception $e) {	
            echo $e->getMessage();
            die;
        }
    }
    final function rowCount()
    {
        try{	
            // execute PDO query
            if (!property_exists($this, 'stm')) {
                $this->find();
            }
            if (property_exists($this, "join")) {
                $this->stm .= "\r\n" . $this->join;
            }
            if (property_exists($this, 'where')) {
                $this->stm .= "\r\nWHERE\r\n\t" . $this->where;
            }
            if (property_exists($this, 'group')) {
                $this->stm .= "\r\n" . $this->group;
            }
            if (property_exists($this, 'order')) {
                $this->stm .= "\r\n" . $this->order;
            }
            if (property_exists($this, 'limit')) {
                $this->stm .= "\r\nLIMIT\r\n" . $this->limit;
            }
            if (property_exists($this, "stm")) {
                $stmt = $this->connect()->prepare($this->stm);
                unset($this->stm);
            }
           
            if ($stmt->execute()) {
                $this->activeRow = $this->rowCount($stmt);
                return $this;
            } else {
                if ($this->dev) {
                    var_dump($stmt);
                    die;
                } else {
                    return json_encode(['error' => true, 'msg' => 'sorry, please check you information and try again.']);
                }
            }
        } catch (Exception $e) {	
            echo $e->getMessage();
            die;
        }
    }
    final function dump()
    {
        // execute PDO query
        if (!property_exists($this, 'stm')) {
                $this->find();
        }
        if (property_exists($this, "join")) {
            $this->stm .= "\r\n" . $this->join;
            unset($this->join);
        }
        if (property_exists($this, 'where')) {
            $this->stm .= "\r\nWHERE\r\n\t" . $this->where;
            unset($this->where);
        } 
        if (property_exists($this, 'group')) {
            $this->stm .= "\r\n" . $this->group;
            unset($this->group);
        }
        if (property_exists($this, 'order')) {
            $this->stm .= "\r\n" . $this->order;
            unset($this->order);
        }
        if (property_exists($this, 'limit')) {
            $this->stm .= "\r\nLIMIT\r\n" . $this->limit;
            unset($this->limit);
        }
        if (property_exists($this, "stm")) {
            $stmt = $this->connect()->prepare($this->stm);
            unset($this->stm);
        }
        
        var_dump($stmt);
        die;
    }
    final function is_null($column)
    {
        if (property_exists($this, 'where')){
            $this->where .= " AND $column IS NULL";
        } else {
            $this->where = " $column IS NULL";
        }

        return $this;
    }
    final static function in()
    {
        $args = func_num_args();
        for ($i = 0;$i < $args;$i++) {
            $params[] = func_get_arg($i);
        }

        return "\r\nIN\r\n\t(".implode(',', $params).")";
    }
    final function is_not_null($column)
    {
        if (property_exists($this, 'where')){
            $this->where .= " AND $column IS NOT NULL";
        } else {
            $this->where = " $column IS NOT NULL";
        }

        return $this;
    }
    final function fetch($obj) 
    {
        if (property_exists($this, "fetchType")) {
            return call_user_func_array(array($this, $this->fetchType), array($obj));
        } else {
            $fetch = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $fetch;
        }
    }
    final function keypair($obj)
    {
        return $obj->fetchAll(PDO::FETCH_KEY_PAIR);
    }
    final function fetch_assoc($obj)
    {
        return $obj->fetchAll(PDO::FETCH_ASSOC);
    }
    final function or($callback) 
    {
        $this->lexpl = "OR";
        $orexpl = $callback($this);
        if (is_object($orexpl)) {
            $sql = $orexpl->where;
        } else {
            $sql = $orexpl;
        }
        $this->lexpl = "AND";

        $this->where = "(" . $sql . ")";
        return $this;
    }
    final static function date()
    {
        $args = func_num_args();
        for ($i = 0;$i < $args;$i++) {
            $params[] = func_get_arg($i);
        }

        $function = $params[0];
        if ($function !== "") {
            $function = "DATE_" . strtoupper($function);
        }

        $str = $function."(".$params[1].", ".$params[2].")";
        return $str;
    }
    public function toString()
    {
        // execute PDO query
        $this->find();
        if (property_exists($this, "join")) {
            $this->stm .= "\r\n" . $this->join;
            unset($this->join);
        }
        if (property_exists($this, 'where')) {
            $this->stm .= "\r\nWHERE\r\n\t" . $this->where;
            unset($this->where);
        } 
        if (property_exists($this, 'group')) {
            $this->stm .= "\r\n" . $this->group;
            unset($this->group);
        }
        if (property_exists($this, 'order')) {
            $this->stm .= "\r\n" . $this->order;
            unset($this->order);
        }
        if (property_exists($this, 'limit')) {
            $this->stm .= "\r\nLIMIT\r\n" . $this->limit;
            unset($this->limit);
        }
        // if (property_exists($this, "stm")) {
        //     $stmt = $this->connect()->prepare($this->stm);
        //     unset($this->stm);
        // }

        return $this->stm;
    }
    final static function subQuery($callable)
    {
        $callabled = $callable();
        if (is_object($callabled)) {
            $sql = $callabled->toString();
        } else {
            $sql = $callabled;
        }
        return "(".$sql.")";
    }
    static function get_params()
    {
        $args = func_num_args();
        for ($i = 0;$i < $args;$i++) {
            $params[] = func_get_arg($i);
        }
        return $params;
    }
}