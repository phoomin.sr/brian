<?php

use app\database\routes;
use controllers\knowledgeController;
use controllers\uploadController;
use providers\request\Request;
use providers\view\Views;

$router->get('/', [controllers\userController::class, 'show_signin']);
$router->get('signup', [controllers\userController::class, 'show_signup']);
$router->get('logout', [controllers\userController::class, 'signout']);
$router->post('signup', [controllers\userController::class, 'register']);
$router->post('login', [controllers\userController::class, 'login']);

$router->post('upload', [controllers\uploadController::class, 'store']);

$router->get('/knowledge/create', [controllers\knowledgeController::class, 'show_knowledge_form']);
$router->get('/update/{id:\d+}', [controllers\knowledgeController::class, 'knowledge_form_update']);
$router->get('folder', [controllers\knowledgeController::class, 'show_user_content']);

$router->post('knowledge/create', [controllers\knowledgeController::class, 'store']);
$router->update(['knowledge/update/{id:\d+}', 'updateKnowledge'], [controllers\knowledgeController::class, 'update']);
$router->delete('knowledge/delete/{id:\d+}', [controllers\knowledgeController::class, 'delete']);

$router->get('knowledge/{set}/{master}?/{header}?', function ($set, $master = null, $header = null) {
    return knowledgeController::set($set, $master, $header);
});

$router->post(['search', 'search'], [controllers\knowledgeController::class, 'search']);
$router->get(['search/{key:c}', 'search_result'], [controllers\knowledgeController::class, 'search_result']);
$router->get(['show_content/{id:i}', 'show_content'], [controllers\knowledgeController::class, 'show_content']);

// $router->get('test/{name}?', function ($name) {
//     return 'hello world mr.' . $name;
// });
