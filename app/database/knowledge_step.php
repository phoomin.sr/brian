<?php namespace app\database;

class knowledge_step extends models {
    protected $table = "knowledge_step";
    protected $fillable = [
        "knowledges_id",
        "description",
        "upload_file_id"
    ];
}