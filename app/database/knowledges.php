<?php namespace app\database;

class knowledges extends models {
    protected $table = "knowledges";
    protected $fillable = [
        "title",
        "description",
        "upload_file_id",
        "conclude",
        "tag",
        "users_id"
    ];
    protected $timestamps = true;
}