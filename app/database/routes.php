<?php namespace app\database;

class routes extends models {
    protected $table = "routes";
    protected $fillable = [
        'route_name', 'class', 'function'
    ];

    function findname($name) {
        return $this->where("route_name", "=", $name)->get()[0];
    }
}