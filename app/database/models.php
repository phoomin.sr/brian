<?php namespace app\database;
require_once dirname(__DIR__, 2) . "/providers/registers.php";

use providers\models\models as ModelsModels;

class models extends ModelsModels {
    protected $db_host = DB_HOST;
    protected $db_name = DB_NAME;
    protected $db_user = DB_USER;
    protected $db_password = DB_PASSWORD;
}