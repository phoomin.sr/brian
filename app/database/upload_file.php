<?php namespace app\database;

class upload_file extends models {
    protected $table = "upload_file";
    protected $fillable = [
        "type",
        "location",
        "filesize",
        "dotfile",
        "filename",
    ];
    protected $timestamps = true;
}