<?php namespace app\database;

class users extends models {
    protected $table = "users";
    protected $fillable = [
        'username',
        'user_email',
        'user_password',
        'firebase_user_id',
        'user_auth',
        'user_status'
    ];
    public $timestamps = true;

    function checkdup($username, $checkrole = false) {
        $user = $this->select('count(id) AS count')
        ->where('username', '=', $username);

        if (is_numeric($checkrole)) {
            $user->where('user_auth', '=', $checkrole);
        }

        $this->checkdup_status = $user->get()[0]['count'] > 0 ? false:true;

        return $this;
    }

    function create_new_account($data) {
        if (!$this->checkdup_status) {
            return ['status' => 'false', 'code' => 200, 'msg' => "ชื่อผู้ใช้นี้มีอยู่แล้ว"];
        }
        if (!$data['emailVerified']) {
            $status = 0;
        } else {
            $status = 1;
        }

        $this->id = $this->insert([
            "username" => $data['username'],
            "user_email" => $data['userEmail'],
            "user_password" => md5($data['password']),
            "firebase_user_id" => $data['uid'],
            "user_auth" => $data['user_auth'],
            "user_status" => $status
        ]);

        return $this;
    }
}