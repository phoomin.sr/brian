<?php namespace controllers;

use app\database\users;
use Illuminate\Support\Facades\Redirect;
use providers\request\Request;
use providers\routes\routeController;
use providers\view\Views;

class userController {
    public const position_signin = [
        "front_signin" => 10,
        "admin_signin" => 99,
    ];
    public const ROLE = [
        10 => "user",
        99 => "admin"
    ];
    function show_signup() {
        return view("signup");
    }
    function show_signin(Request $request, routeController $route) {
        $session = $request->session();

        if ($session->get('user_id') !== null) {
            $view = view('home', [
                'search' => $route->route('search'),
            ]);
            $view->Auth($session->get('username'), self::ROLE[$session->get('role')]);
        } else {
            $view = view("signin");
        }
        return $view;
    }
    function signout(Request $request) {
        $request->session()->unset();

        return redirect_to('./');
    }
    function login(Request $request) {
        $post = $request->getAll();
        $user = new users;

        $uinfo = $user->where('firebase_user_id', '=', $post['uid'])
        ->get();

        if (!empty($uinfo)) {
            $this->set_session($uinfo[0]['id']);
            return response(['status' => true, 'msg' => 'success to signin!']);
        } else {
            return response(['status' => false, 'msg' => 'username or password is incorrect.']);
        }
    }
    function register(Request $request){
        $response = $this->create_account($request);
        
        // auto signin
        if ($response['status']) {
            $user = new users;
            $uinfo = $user->where('username', '=', $request->get('username'))
            ->where('user_password', '=', md5($request->get('password')))
            ->get();

            $this->set_session($uinfo[0]['id']);
        }

        return response($response);
    }
    function create_account(Request $request) {
        $user = new users;
        $data = $request->getAll();

        $data['user_auth'] = self::position_signin[$data['user_role']];

        $user->checkdup($data['username'], $data['user_auth'])->create_new_account($data);

        if (isset($user->id)) {
            $status = true;
            $msg = "created!";
        } else {
            $status = false;
            $msg = "has something mistake. please check all input and try again";
        }

        return ['status' => $status, 'msg' => $msg];
    }
    function set_session($user_id) {
        $request = new Request;
        $session = $request->session();

        $user = new users;
        $uinfo = $user->find($user_id);

        $session->set('user_id', $uinfo['id'])
        ->set('username', $uinfo['username'])
        ->set('role', $uinfo['user_auth']);
    }
}
