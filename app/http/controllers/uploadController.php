<?php namespace controllers;

use app\database\upload_file;
use providers\request\Request;

class uploadController {
    function store(Request $request){
        $upload = new upload_file;
        $dir = dirname(__DIR__, 3);
        $target_dir = $dir . "/resources/images";
        $post = $request->getFiles();
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0755, true);
        }
        foreach ($post as $key => $item) {
            if (is_array($item['name'])) {
                for ($i = 0;$i < count($item);$i++) {
                    if (isset($item['name'][$i])) {
                        $target_file = $target_dir . '/' . $item['name'][$i];
                        move_uploaded_file($item['tmp_name'][$i], $target_file);
                        $id = $upload->insert([
                            "type" => $item['type'][$i],
                            "location" => "/resources/images" . '/' . $item['name'][$i],
                            "filesize" => $item['size'][$i],
                            "dotfile" => $item['type'][$i],
                            "filename" => $item['name'][$i]
                        ]);
                    }
                }
            } else {
                $target_file = $target_dir . '/' . $item['name'];
                move_uploaded_file($item['tmp_name'], $target_file);
                $id = $upload->insert([
                    "type" => $item['type'],
                    "location" => "/resources/images" . '/' . $item['name'],
                    "filesize" => $item['size'],
                    "dotfile" => $item['type'],
                    "filename" => $item['name']
                ]);
            }
        }
        return $id;
    }
}