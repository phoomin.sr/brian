<?php namespace controllers;
use providers\view\Views;

class record {
    static function create_audio_recorder($id, $name) {
        return Views::render("component.audio_recorder", ['id' => $id, 'name' => $name])->run();
    }   
}