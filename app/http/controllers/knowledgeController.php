<?php namespace controllers;
require_once dirname(__DIR__, 3) . "/index.php";

use app\database\contents;
use app\database\knowledge_step;
use app\database\knowledges;
use app\database\users;
use Phroute\Phroute\RouteParser;
use providers\request\Request;
use providers\routes\Route;
use providers\routes\routeController;
use providers\view\Views;

class knowledgeController {
    static function set($set, $master = null, $header = null) {
        $content = "";
        $getKnow = new knowledgeController();
        $knowledge = new knowledges;
        $getContent = new contents;

        if (!is_null($header)) {
            $header = $getContent
            ->where('header', '=', $header)
            ->get();

            $content = $getKnow->createContent($header);
        } else if (!is_null($master)) {
            $menulist = $knowledge
            ->where('set', '=', $set)
            ->where('master', '=', $master)
            ->get();

            $content = $getKnow->createMenu($menulist);
        } else {
            $menulist = $knowledge
            ->where('set', '=', $set)
            ->get();

            $content = $getKnow->createMenu($menulist);
        }

        return $content;
    }
    function show_knowledge_form(Request $request, routeController $route)
    {
        $uid = $request->session()->get('user_id');

        return view('knowledge_form', [
            'title' => 'how-to',
            'user_id' => $uid,
            'step' => array(),
            'action' => $route->getRoute('knowledge/create'),
            'method' => 'POST'
        ]);
    }
    function createContent(Request $request) {

    }
    function createMenu($menulist) {

    }
    function show_user_content(Request $request) 
    {
        $user_id = $request->session()->get('user_id');

        $kn = new knowledges;
        $res = $kn
        ->select("knowledges.*", "upload_file.location")
        ->join('upload_file', 'upload_file.id', 'knowledges.upload_file_id')
        ->where('users_id', '=', $user_id)
        ->get();

        $user = new users;
        $uinfo = $user->find($user_id);

        return view('folder_view', [
            'username' => $uinfo['username'],
            'data' => array_chunk($res, 3),
        ]);
    }
    function knowledge_form_update(Request $request, $id, routeController $route)
    {
        $kn = new knowledges;
        $step = new knowledge_step;

        $uid = $request->session()->get("user_id");

        $info = $kn
        ->select('knowledges.*', 'upload_file.location')
        ->join('upload_file', 'upload_file.id', 'knowledges.upload_file_id')
        ->where("knowledges.id", "=", $id)
        ->get()[0];
        
        $dt = $step
        ->select('knowledge_step.*', 'upload_file.location')
        ->join('upload_file', 'upload_file.id', 'knowledge_step.upload_file_id')
        ->where('knowledge_step.knowledges_id', '=', $id)
        ->get();

        return view('knowledge_form', [
            "info" => $info,
            "step" => $dt,
            "method" => "PUT",
            'title' =>  "Edit :: " . $info['title'],
            'user_id' => $uid,
            'action' => $route->route('knowledge/update', [$id]),
        ]);
    }
    function store(Request $request) 
    {
        $post = $request->getAll();

        $kn = new knowledges;
        $knowledge_id = $kn->insert([
            "title" => $post['title'],
            "description" => $post['hint'],
            "upload_file_id" => $post['top_image'],
            "conclude" => $post['sumary'],
            "tag" => $post['tag'],
            "users_id" => $request->session()->get('user_id')
        ]);

        for ($i = 0;$i < count($post['order']);$i++) {
            if ($post['order'][$i] !== "") {
                $step = new knowledge_step;
                $step->insert([
                    "knowledges_id" => $knowledge_id,
                    "description" => $post['order'][$i],
                    "upload_file_id" => $post['order_step'][$i]
                ]);
            }
        }

        return json_encode(['store' => true, 'msg' => 'เพิ่มเนื้อหาใหม่เรียบร้อยแล้ว!']);
    }
    function update(Request $request, $id) 
    {
        $data = $request->getAll();
        $dt = $data['order'];

        $kn = new knowledges;
        $kn->update([
            "title" => $data['title'],
            "description" => $data['hint'],
            "conclude" => $data['sumary'],
            "tag" => $data['tag'],
            "users_id" => $request->session()->get('user_id')
        ])
        ->where('id', '=', $id)
        ->get();

        $step = new knowledge_step;
        $step->delete()->where("knowledges_id", "=", $data['id'])->get();

        for ($i = 0;$i < count($dt);$i++) {
            if ($dt[$i] !== "") {
                $step = new knowledge_step;
                $step->update([
                    "knowledges_id" => $data['id'],
                    "description" => $dt[$i],
                    "order_step" => $data['order_step'][$i]
                ])
                ->where('knowledges_id', '=', $data['id'])
                ->get();
            }
        }

        return json_encode(['update' => true, 'msg' => 'อัพเดทเนื้อหาเรียบร้อยแล้ว']);
    }
    function search (Request $request, routeController $route)
    {
        $search = $request->get('text');
        $str_string = prep_search($search);

        return json_encode(['redirect' => $route->route('search', [$str_string])]);
    }
    function search_result(Request $request, $key, routeController $route)
    {
        $keysearch = prep_search($key, true);
        $this->search = str_replace(' ', '%', $keysearch);

        $this->get_content_byTitle();
        $this->get_content_byBody();
        $this->get_content_byTag();

        $contents = $this->ContentGetLink($this->AllContent, $route);

        return view('result_view', [
            "keysearch" => implode(' ', $keysearch),
            "result" => $contents,
            "search" => $route->route('search')
        ]);
    }
    function get_content_byTitle($point = 30)
    {
        $kn = new knowledges;
        $kn->or(function ($kn){
            for($i = 0;$i < count($this->search);$i++) {
                $kn->where('title', 'like', '%' . $this->search[$i] . '%');
            }
            return $kn;
        });
        $kn->or(function ($kn) {
            for($i = 0;$i < count($this->search);$i++) {
                $kn->where('description', 'like', '%' . $this->search[$i] . '%');
            }
            return $kn;
        });

        $kn->order('create_at', 'z-a');
        
        $this->AllContent["withTitle"] = $kn->get();
    }
    function get_content_byBody($point = 20)
    {
        $kn = new knowledge_step;
        $kn->or(function ($kn) {
            for($i = 0;$i < count($this->search);$i++) {
                $kn->where('description', 'like', '%' . $this->search[$i] . '%');
            }
            return $kn;
        });
        
        $this->AllContent["withBody"] = $kn->get();
    }
    function get_content_byTag($point = 10)
    {
        $kn = new knowledges;
        $kn->or(function ($kn) {
            for($i = 0;$i < count($this->search);$i++) {
                $kn->where('tag', 'like', "%" . $this->search[$i] . "%");
            }
            return $kn;
        });

        $kn->order('create_at', 'z-a');

        $this->AllContent["withTag"] = $kn->get();
    }
    function ContentGetLink($contents, $route)
    {
        $content = array();
        foreach($contents as $key=>$item) {
            for ($i = 0;$i < count($item);$i++) {
                $link = $route->route('show_content', [$item[$i]['id']]);
                $content[] = [
                    "title" => $item[$i]['title'],
                    "description" => $item[$i]['description'],
                    "link" => $link
                ];
            }
        }

        return $content;
    }
    function show_content(Request $request, $id) {
        $kn = new knowledges;
        $step = new knowledge_step;

        $head = $kn
        ->join('upload_file', 'upload_file.id', 'knowledges.upload_file_id')
        ->where('knowledges.id', '=', $id)
        ->get()[0];

        $order = $step
        ->join('upload_file', 'upload_file.id', 'knowledge_step.upload_file_id')
        ->where("knowledges_id", "=", $id)
        ->get();

        return view('show_content', [
            "head" => $head,
            "step" => $order,
        ]);
    }
}