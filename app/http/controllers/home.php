<?php namespace controllers;

use app\database\users;
use providers\view\Views;
use models\base;

class home {
    public function index() 
    {
        return view('home', array());
    }
}