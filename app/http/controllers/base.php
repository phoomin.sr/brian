<?php namespace controllers;

abstract class base {
    public const AUTO_RUN_FUNC_BYMETHOD = [
        'POST' => 'save',
        'GET' => 'get',
        'UPDATE' => 'update',
        'DELETE' => 'delete'
    ];
    public function run_func_bymethod()
    {
        $func = self::AUTO_RUN_FUNC_BYMETHOD[strtoupper($_SERVER['REQUEST_METHOD'])];
    }
}