// //webkitURL is deprecated but nevertheless 
URL = window.URL || window.webkitURL;
var gumStream;
// //stream from getUserMedia() 
var rec;
// //Recorder.js object 
var input;
// //MediaStreamAudioSourceNode we'll be recording 
// // shim for AudioContext when it's not avb. 
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext = new AudioContext;
// //new audio context to help us record 
// var recordButton = document.getElementById("recordButton");
// var stopButton = document.getElementById("stopButton");
// var pauseButton = document.getElementById("pauseButton");
// //add events to those 3 buttons 
// recordButton.addEventListener("click", startRecording);
// stopButton.addEventListener("click", stopRecording);
// pauseButton.addEventListener("click", pauseRecording);

// function startRecording() { console.log("recordButton clicked"); }
recordButton = $('#recordButton');
stopButton = $('#stopButton');
pauseButton = $('#pauseButton');
stopButton.hide();
pauseButton.hide();

$(document).ready(function () {
    recordButton.click(function (e) {
        e.preventDefault();
        startRecording();
        recordButton.hide();
        stopButton.show();
        pauseButton.show();
    });
    pauseButton.click(function (e) {
        e.preventDefault();
        pauseRecording();
    });
    stopButton.click(function (e) {
        e.preventDefault();
        stopRecording();
    })
})

function startRecording() {
    var constraints = {
        audio: true,
        video: false
    } 
    navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
        console.log("getUserMedia() success, stream created, initializing Recorder.js ..."); 
        /* assign to gumStream for later use */
        gumStream = stream;
        /* use the stream */
        input = audioContext.createMediaStreamSource(stream);
        /* Create the Recorder object and configure to record mono sound (1 channel) Recording 2 channels will double the file size */
        rec = new Recorder(input, {
            numChannels: 1
        }) 
        //start the recording process 
        rec.record();
        console.log("Recording started");
    });
}

function pauseRecording () {
    console.log(rec.recording);
    if (rec.recording) {
        rec.stop();
        pauseButton.html('<i class="fas fa-play"></i>');
    } else {
        rec.record();
        pauseButton.html('<i class="fas fa-pause"></i>');
    }
}

function stopRecording () {
    console.log("stop!");
    rec.stop();
    gumStream.getAudioTracks()[0].stop();
    recordButton.show();
    stopButton.hide();
    pauseButton.hide();
    rec.exportWAV(createDownloadLink);
}

function createDownloadLink(blob) {
    var url = URL.createObjectURL(blob);
    var au = document.createElement('audio');
    var li = document.createElement('li');
    var link = document.createElement('a');
    //add controls to the <audio> element 
    au.controls = true;
    au.src = url;
    //link the a element to the blob 
    link.href = url;
    link.download = new Date().toISOString() + '.wav';
    link.innerHTML = link.download;
    //add the new audio and a elements to the li element 
    li.appendChild(au);
    li.appendChild(link);
    //add the li element to the ordered list 
    recordingsList.appendChild(li);
}
