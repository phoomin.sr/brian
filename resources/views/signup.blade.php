@extends('layouts.apps')

@section('contents')
<div class="container-fluid d-flex justify-content-center">
    <div class="bg-white mt-5 mb-5 p-3 rounded" style="width: 23rem">
        <form action="./signup" method="post" id="register">
            <h1 class="text-center">เข้าสู่ระบบ</h1>
            <hr>
            <div class="mt-4 mb-3">
                <label for="userEmail" class="form-label">อีเมล </label>
                <input type="email" name="userEmail" id="userEmail" class="form-control">
                <span class="text-danger">*กรุณาใช้ email ที่สามารถยืนยันตัวตนได้จริง</span>
            </div>
            <div class="mt-4 mb-3">
                <label for="username" class="form-label">ชื่อผู้ใช้</label>
                <input type="text" name="username" id="username" class="form-control">
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">รหัสผู้ใช้</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>
            <div class="mb-3">
                <label for="confirm_password" class="form-label">ยืนยันรหัสผู้ใช้</label>
                <input type="password" name="confirm_password" class="form-control" id="confirm_password">
            </div>
            <div class="mb-3">
                <input type="hidden" name="user_role" id="user_role" value="front_signin">
                <input type="submit" value="Sign up" class="btn btn-block btn-primary form-control">
            </div>
            <div class="mb-3">
                <a class="btn btn-block btn-secondary form-control" href="./">< back</a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
    <script>
        function validate(data) {
            if (data.confirm_password == "") {
                swal('sorry!', 'confirm password can\'t be null.', 'error');
                return false;
            }

            if (data.confirm_password !== data.password) {
                swal('sorry!', 'confirm password is\'t match.', 'error');
                return false;
            }

            return true;
        }
        async function submit_register(data, url) {
            if (!validate(data)) {
                return ;
            }
            // var user = signupWithFirebaseEmailandPassword(data.userEmail, data.password);
            const firebase = firebaseConnect();
            await firebase.auth().createUserWithEmailAndPassword(data.userEmail, data.password)
            .then((userCredential) => {
                // Signed in
                const user = userCredential.user;
                data.uid = user.uid;
                data.emailVerified = user.emailVerified;

                if (!user.emailverify) {
                    sendEmailverified(data.username);
                    return;
                }

                send(url, "POST", data)
                .then(function (response) {
                    if (response.status) {
                        swal('created!', response.msg, 'success')
                        .then(function () {
                            window.location.replace("{{ route::getRoute('/') }}");
                        });
                    } else {
                        swal('sorry!', response.msg, 'error');
                    }
                })
                .catch(function (err) {
                    console.log(err);
                });
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                swal(errorCode, errorMessage, 'error');
            });
            
        }
    </script>
@endsection