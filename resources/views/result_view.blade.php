@extends('layouts.apps')

@section('contents')
    <div class="container text-white">
        <div class="row mt-2">
            <div class="col-3">
                <h4 class="h4">Brain search</h4>
            </div>
            <div class="col">
                <input type="search" name="search" id="search" value="{{ $keysearch }}" class="form-control">
            </div>
            <div class="col-2">
                <button id="search-butt" class="btn btn-block btn-primary"><i class="fas fa-search"></i></button>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col ">
                <a class="nav-link text-light" href="{{ route::getRoute('knowledge/create') }}"><i class="fas fa-plus h4 d-flex justify-content-center"></i><p class="text-center">เพิ่ม know-how ใหม่</p></a>
                
            </div>
            <div class="col">
                <a class="nav-link text-light" href="{{ route::getRoute('folder') }}"><i class="far fa-folder h4 d-flex justify-content-center"></i><p class="text-center">know-how ทั้งหมดของฉัน</p></a>
            </div>
        </div>
        <hr>
            <p class="mb-3"> ผลการค้นหา:</p>
            @for ($i = 0; $i < count($result); $i++)
                <h4>{{ $result[$i]['title'] }}</h4>
                <a href="{{ $result[$i]['link'] }}">{{ $result[$i]['link'] }}</a>
                <div class="row">
                    <div class="col"><p>{{ $result[$i]['description'] }}</p></div>
                </div>
                <hr>
            @endfor
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#search-butt').click(function () {
                let text = $('#search').val();
                send('{{ $search }}', 'POST', { 'text': text })
                .then(response => {
                    window.location.replace(response.redirect);
                })
                .catch(err => {
                    console.log(err);
                });
            });
            $('body').bind('keyup, keypress', function (e) {
                if (e.keyCode == 13) {
                    $('#search-butt').trigger('click');
                }
            })
        });
    </script>
@endsection