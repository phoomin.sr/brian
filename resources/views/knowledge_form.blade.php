@extends('layouts.apps')

@section('contents')
    <div class="d-flex justify-content-center bg-white m-5 p-2 rounded">
        <div class="container">
            <h1 class="mt-2">{{ $title }}</h1>
            <hr>
            <form action="{{ $action }}" method="{{ $method }}" id="knowledge" enctype="multipart/form-data">
                <div class="row mt-3 mb-3">
                    <div class="col">
                        <label for="title" class="form-label">หัวข้อ</label>
                        <input type="text" name="title" id="title" placeholder="หัวข้อเรื่องที่จะเพิ่ม..." class="form-control"
                            value="{{ $info['title'] ?? "" }}"
                        >
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        @if (isset($info['location']))
                            <img class="img-fluid img-thumbnail rounded mx-auto d-block" src="../{{ $info['location'] }}" alt="{{ $info['title'] }}">
                        @else
                            <input type="file" name="top_image" id="top_image" accept="image/*" class='filepond'>
                        @endif
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col">
                        <label for="hint" class="form-label">เกริ่นนำ</label>
                        <textarea name="hint" id="hint" class="form-control" placeholder="เกริ่นนำ">{{ $info['description'] ?? "" }}</textarea>
                    </div>
                </div>
                <h4>ขั้นตอนการทำ</h4>
                <div id="step" class="mb-5 p-2 rounded border">
                    @for ($i = 0; $i < max(count($step), 5); $i++)
                        <div class="row">
                            <div class="col p-3">
                                <label for="order" class="form-label">ขั้นตอนที่ {{ $i+1 }}</label>
                                @if (isset($step[$i]['location']))
                                    <img class="img-fluid img-thumbnail rounded mx-auto d-block" style='max-width:40%; height: auto' src="../{{ $step[$i]['location'] }}">
                                @else
                                    <input type="file" name="order_step[]" id="order_step" accept="image/*" class='filepond'>
                                @endif
                                    <textarea type="text" name="order[]" class="form-control" value="{{ $step[$i]['description'] ?? "" }}"></textarea>
                                @if (isset($step[$i]['id']))
                                    <input type="hidden" name="order_id[]" id="order_id" value="{{ $step[$i]['id'] }}">
                                @endif
                            </div>
                        </div>
                    @endfor
                    <div class="row plus-order">
                        <div class="col p-3 d-flex justify-content-center">
                            <button id="plus-order" class="btn btn-primary rounded rounded-circle"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="sumary" class="form-label">สรุป</label>
                        <textarea name="sumary" id="sumary" class="form-control">{{ $info['conclude'] ?? "" }}</textarea>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="tag" class="form-label">เพิ่มแท็ก</label>
                        <input type="text" name="tag" id="tag" class="form-control" 
                        placeholder="ใส่ # หน้า tag และคั่นด้วยเครื่องหมาย , ในแต่ละแท็ก"
                        value="{{ $info['tag'] ?? '' }}"
                        >
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <input type="hidden" name="id" value="{{ $info['id'] }}">
                        <input type="submit" value="ส่ง" class="form-control btn btn-block btn-primary">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <a class="btn btn-block btn-secondary form-control" href="../folder">< Back</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('button#plus-order').click(function (e) {
                e.preventDefault();
                let cloned = '<div class="row">'
                        + '<div class="col p-3">'
                        + '<label for="order" class="form-label">ขั้นตอนที่ ' + ($('#step>div').length) + '</label>'
                        + '<input type="file" name="order_step[]" id="order_step" class="filepond">'
                        + '<input type="text" name="order[]" class="form-control">'
                        + '</div>'
                        + '</div>';

                $('#step>div.plus-order').before(cloned);
                FilePond.parse(document.body);
                FilePond.setOptions({
                    server: "{{ route::getRoute('upload') }}"
                });
            });
        });

        function submit_knowledge(data, url) {
            // add picture from filepond
            data['order_step'] = [];
            data['order'] = [];
            data['order_id'] = [];
            $('input[type=hidden][name="order_step[]"]').each(function () {
                data['order_step'].push($(this).val());
            });
            $('[name="order[]"]').each(function () {
                data['order'].push($(this).val());
            });
            if ($('input[name="order_id[]"]').length > 0) {
                $('input[name="order_id[]"]').each(function () {
                    data['order_id'].push($(this).val());
                });
            }

            send(url, '{{ $method ?? 'POST' }}', data)
            .then(function (response) {
                swal('Congratulation', response.msg, 'success')
                .then(function () {
                    window.location.reload();
                });
            })
            .catch(function (err) {
                console.log(err);
            });
        }
    </script>
@endsection