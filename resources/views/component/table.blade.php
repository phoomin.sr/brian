<table class="table">
    <tr>
        @foreach ($head as $item)
            <th>{{ $item }}</th>
        @endforeach
    </tr>
    @for ($i = 0; $i < count($recs); $i++)
        <tr>
            <td><p data-edit='true' data-id="{{ $recs[$i]['id'] }}" data-target="name">{{ $recs[$i]['name'] }}</p></td>
            <td><p data-edit='true' data-id="{{ $recs[$i]['id'] }}" data-target="create_at">{{ $recs[$i]['create_at'] }}</p></td>
            <td><input type="button" value="ลบ" class="btn btn-block btn-danger"></td>
        </tr>
    @endfor
</table>  