@extends('layouts.apps')

<div class="row mt-1">
    <div class="col d-flex justify-content-center">
        <button id="{{ $id }}-recorder" class="btn btn-info rounded rounded-circle"><i class="fas fa-microphone-alt text-white"></i></button>    
        <button id="{{ $id }}-pause" class="btn btn-warning rounded rounded-circle d-none"><i class="fas fa-pause text-white"></i></button>
        <button id="{{ $id }}-play" class="btn btn-warning rounded rounded-circle d-none"><i class="fas fa-play text-white"></i></button>
        <button id="{{ $id }}-stop" class="btn btn-danger rounded rounded-circle d-none"><i class="fas fa-stop text-white"></i></button>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="show_list_{{ $id }}"></div>
    </div>
</div>

@section('script')
    <script>
        $(document).ready(function () {
        recbut = $('button[id$="recorder"]');
        pausebut = $('button[id$="pause"]');
        playbut = $('button[id$="play"]');
        stopbut = $('button[id$="stop"]');
        console.log(stopbut);
        showlist = $('[class^="show_list"]');

        recbut.click(function () {
            recbut.addClass('d-none');
            pausebut.removeClass('d-none');
        });
        pausebut.click(function () {
            pausebut.addClass('d-none');
            playbut.removeClass('d-none');
            stopbut.removeClass('d-none');
        });
        playbut.click(function () {
            playbut.addClass('d-none');
            stopbut.addClass('d-none');
            pausebut.removeClass('d-none');
        });
        stopbut.click(function(){
            stopbut.addClass('d-none');
            playbut.addClass('d-none');
            recbut.removeClass('d-none');
        });
        });
    </script>
@endsection
