@extends('layouts.apps')

@section('contents')
    <div class="container text-white">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                <a class="navbar-brand text-white" href="#">Resolute Management and Service</a>
            </div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link text-light" href="{{ route::getRoute('knowledge/create') }}"><i class="fas fa-plus h4"></i></a>
                    </li>
                    <li>
                        <a class="nav-link text-light" href="{{ route::getRoute('folder') }}"><i class="far fa-folder h4"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-light" href="{{ route::getRoute('logout') }}"><i class="fas fa-sign-out-alt h4"></i></a>
                    </li>
                </ul>
            </div>
          </nav>
        <div class="row">
            <div class="col">
                @auth('pop')
                    <a href="./structure/cola-engine" class="d-flex justify-content-end text-decoration-none text-white mb-2">โครงสร้าง minscale Cola-engine</a>
                @endauth
                <a href="./erp/quickstart" class="d-flex justify-content-end text-decoration-none text-white mb-2">เรียนรู้พื้นฐาน ERP (สำหรับโปรแกรมเมอร์)</a>
                <a href="./seo/quickstart" class="d-flex justify-content-end text-decoration-none text-white">เรียนรู้พื้นฐาน SEO (สำหรับนักการตลาดดิจิตอล)</a>
            </div>
        </div>
        <div class="row" style="min-height: 50vh">
            <div class="col">
                <div class="row align-middle" style="margin-top: 25vh">
                    <h1 class="text-center mb-5">KNOWLEDGE BRAIN PROJECT</h1>
                    <div class="col">
                        <div class="mb-3">
                            <input type="search" name="global-search" id="global-search" class="form-control rounded rounded-pill" placeholder="search knowledge base?">
                        </div>
                        <div class="d-flex justify-content-center">
                            <input class="btn btn-md btn-primary rounded rounded-pill" id="search" type="button" value="search">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('body').bind("keyup, keypress", function (e) {
            if (e.keyCode == 13) {
                $('#search').trigger('click');
            }
        });
        $('#search').click(function () {
            let text = $('#global-search').val();
            send('{{ $search }}', 'POST', { 'text': text })
            .then(response => {
                window.location.replace(response.redirect);
            })
            .catch(err => {
                console.log(err);
            });
        });
    });
</script>
@endsection