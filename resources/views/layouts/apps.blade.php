<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ route::getRoute('vendor/components/font-awesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ route::getRoute('node_modules/filepond/dist/filepond.min.css') }}">
    <link
        href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css"
        rel="stylesheet"
    />
    <link
        href="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css"
        rel="stylesheet"
    />

    <!-- daterange picker -->
    {{-- <link rel="stylesheet" href="{{ route::getRoute('public/daterangepicker-master/daterangepicker.css')}}"> --}}

    @yield('style')
    <title>{{ $title ?? $appname }}</title>
</head>
<body class="container bg-dark">
    @yield('contents')
    @csrf
    <!-- script -->
    <script src="{{ route::getRoute('vendor/components/jquery/jquery.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    <script src="{{ route::getRoute('node_modules/filepond/dist/filepond.min.js') }}"></script>
    <script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.js"></script>
    <script src="https://cdn.rawgit.com/mattdiamond/Recorderjs/08e7abd9/dist/recorder.js"></script>
    {{-- <script src="./resources/js/audio.js"></script> --}}
    <!-- daterange picker -->
    {{-- <script src="./public/daterangepicker-master/moment.min.js"></script>
    <script src="./public/daterangepicker-master/daterangepicker.js"></script> --}}

    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js"></script>

    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/8.9.1/firebase-analytics.js"></script>

    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/8.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.9.1/firebase-firestore.js"></script>

    <script>
        const firebaseConfig = {
            apiKey: "AIzaSyAn9AXQHmsPoc9fIOHyXgnygJOr5j8B0IU",
            authDomain: "rmsbrain-8c55b.firebaseapp.com",
            projectId: "rmsbrain-8c55b",
            storageBucket: "rmsbrain-8c55b.appspot.com",
            messagingSenderId: "58745417447",
            appId: "1:58745417447:web:cc5f7232e6fea459fe3c42",
            measurementId: "G-NK3J1ESD89"
        };
    </script>
    <script>
        $(document).ready(function () {
            $.fn.filepond.registerPlugin(FilePondPluginImagePreview, FilePondPluginFilePoster);

            FilePond.parse(document.body);
            FilePond.setOptions({
                server: "{{ route::getRoute('upload') }}"
            });

            $('form').submit(function (e) {
                e.preventDefault();
                var formId = $(this).attr('id');
                var formReq = $(this).attr('action');
                var form_data = replace_form_value($('form#'+formId).serializeArray());
                
                funcName = 'submit_' + formId;
                window[funcName](form_data, formReq);
            });
        });
        function stream_recorder() {
            console.log('stream recorder is already in use.');
        }
        function replace_form_value(formvalue) {
            data = {};
            $.each(formvalue, function (key, value) {
                elename = value.name;
                if (elename.search(/\[\]/gm) > 0) {
                    data[value.name] += [value.value];
                } else {
                    data[value.name] = value.value; 
                }
            });
            return data;
        }
        async function send(url = '', type = 'POST', data = {}) {
        const response = await fetch(url, {
            method: type,
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(data)
        });
            return response.json();
        }
        function firebaseConnect() {
            if (!firebase.apps.length) {
                return firebase.initializeApp(firebaseConfig);
            }else {
                return firebase.app(); // if already initialized, use that one
            }
        }
        function sendEmailverified(email) {
            const firebase = firebaseConnect();
            firebase.auth().currentUser.sendEmailVerification()
            .then(() => {
                swal('กรุณายืนยันอีเมลก่อนเข้าใช้งาน', 'ระบบได้ทำการส่งอีเมลยืนยันไปยัง ' + email + 'เรียบร้อยแล้ว</br>กรุณายืนยันตัวตนของคุณ');
            })
        }
    </script>
    @yield('script')
</body>
</html>