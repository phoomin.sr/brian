@extends('layouts.apps')

@section('contents')
    <div class="container-fluid d-flex justify-content-center">
        <div class="bg-white mt-5 mb-5 p-3 rounded" style="width: 23rem">
            <form action="./login" method="post" id="login">
                <h1 class="text-center">เข้าสู่ระบบ</h1>
                <hr>
                <div class="mt-4 mb-3">
                    <label for="username" class="form-label">ชื่อผู้ใช้</label>
                    <input type="text" name="username" id="username" class="form-control">
                </div>
                <div class="mb-1">
                    <label for="password" class="form-label">รหัสผู้ใช้</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <div class="mb-3">
                    <input type="checkbox" name="remember_me" id="remember_me">
                    <label for="remember_me">จำผู้ใช้</label>
                </div>
                <div class="mb-3">
                    <input type="submit" value="Sign In" class="btn btn-block btn-primary form-control">
                </div>
                <div class="mb-3">
                    <a class="btn btn-block btn-secondary form-control" href="./signup">Sign up</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#forget_password').click(function(){
                console.log();
            });
        });
        async function submit_login(data, url) {
            var firebase = firebaseConnect();
            await firebase.auth().signInWithEmailAndPassword(data.username, data.password)
                .then((userCredential) => {
                    // Signed in
                    var user = userCredential.user;
                    data.uid = user.uid;

                    if (!user.emailVerified) {
                        sendEmailverified(data.username);
                        return;
                    }

                    send(url, 'POST', data)
                    .then(function (response) {
                        if (response.status) {
                            swal('Congratulation', response.msg, 'success')
                            .then(function () {
                                window.location.reload();
                            });
                        } else {
                            swal('Sorry!', response.msg, 'error');
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                    });
                })
                .catch((error) => {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    if ($('#forget_password').length == 0) {
                        $('label[for=remember_me]').after("<div><button id='forget_password' class='btn btn-danger btn-block'>forget password?</button></div>");
                    }
                    
                    swal(errorCode, errorMessage, 'error');
                });
        }
    </script>
@endsection