@extends('layouts.apps')

@section('contents')
    <div class="container">
        <div class="row border p-3 m-3 border-white text-white rounded">
            <div class="row mb-3">
                <div class="col-1">
                    <a href="{{ route::getRoute('/') }}" class="btn btn-primary rounded rounded-circle"><i class="fas fa-chevron-left"></i></a>
                </div>
                <div class="col">
                    <h2>{{ $username }}</h2>
                </div>
            </div>
            <hr>
            @foreach ($data as $item)
                <div class="row">
                    @foreach ($item as $dt)
                        <div class="col">
                            <div class="card">
                                <img src=".{{ $dt['location'] }}" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title text-dark">{{ $dt['title'] }}</h5>
                                    <p class="card-text text-dark">{{ $dt['description'] }}</p>
                                    <a href="{{ route::getRoute('update',["id" => $dt['id']]) }}" class="btn btn-primary form-control">edit</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
        {{-- @foreach ($data as $item) --}}
            {{-- @dump($item) --}}
        {{-- @endforeach --}}
    </div>
@endsection