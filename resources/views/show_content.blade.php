@extends('layouts.apps')

@section('contents')
    <div class="mt-4 mb-4 p-3 rounded shadow row bg-white">
        <h2 class="text-bold">{{ $head['title'] }}</h2>
        <div class="row mb-4">
            <div class="col d-flex justify-content-center mt-4">
                <img src="../{{ $head['location'] }}" alt="{{ $head['filename'] }}" class="img-fluid img-thumbnail" style="width:50%; height:auto">
            </div>
        </div>
        <ol class='m-3 p-3'>
            @for ($i = 0; $i < count($step); $i++)
                <div class="row">
                    <div class="col">
                        <li class="pl-3">{{ $step[$i]['description'] }}</li>
                        <div class='d-flex justify-content-center'>
                            <img src="../{{ $step[$i]['location'] }}" alt="{{ $step[$i]['filename'] }}" class="img-fluid img-thumbnail" style="width:25%; height:auto">
                        </div>
                    </div>
                </div>
                <hr>
            @endfor
        </ol>
        <div class="row">
            <div class="col">
                {{ $head['conclude'] }}
            </div>
        </div>
        <div class="row mt-4">
            <div class="col d-flex justify-content-end">
               <p class="blockquote-footer">{{ $head['tag'] }}</p>
            </div>
        </div>
    </div>
@endsection